import React,{useState} from 'react'
import Sender from '../sender/sender'
import Receiver from '../receiver/receiver'
import axios from 'axios';

const Main = () => {

    /*
        Este componente es la vista principal.
        Únicamente contiene componentes, funciones y estados.
    */

    const [resultados, modificarResultados] = useState({
        primerResultado: '',
        segundoResultado: '',
        tercerResultado: ''
    })
    const {primerResultado,segundoResultado,tercerResultado} = resultados;
    
    const [esPalindromo, modificarEsPalindromo] = useState('')

    const [text, modificarText] = useState('')
    const llamaModificarText = e =>{
        modificarText(e.target.value)
    }
    
    const consultarApi = async text =>{
        try {
            const respuesta = await axios.get('http://localhost:4000/iecho?text='+text,{headers: {"Access-Control-Allow-Origin": "*"}});
            if(respuesta.data.text.length){
                modificarResultados({
                    segundoResultado: primerResultado,
                    tercerResultado: segundoResultado,
                    primerResultado: respuesta.data.text
                })
                modificarText('')
                if(respuesta.data?.palindrome){
                    modificarEsPalindromo(respuesta.data.text + ' es palindromo!')
                    setTimeout(() => {
                        modificarEsPalindromo('')
                    }, 2000);
                }else{
                    modificarEsPalindromo('')
                }
            }
        } catch (error) {
            console.log(error)
        }
    } 
    return(
        <div data-testid="main" style={{backgroundColor:'rgb(229,229,229)',height: '100%'}}>
            <Sender llamaModificarText={llamaModificarText} text={text} consultarApi={consultarApi} />
            <Receiver esPalindromo={esPalindromo} primerResultado={primerResultado} segundoResultado={segundoResultado} tercerResultado={tercerResultado} />
        </div>
    )
}

export default Main