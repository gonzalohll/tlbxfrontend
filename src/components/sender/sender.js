import React from 'react'

const Sender = ({consultarApi, text, llamaModificarText}) => {
    /*
        Este componente permite al usuario indicar una palabra y enviarla al servidor para su procesamiento.
        Recibe como props dos funciones y un estado. 
        Text es el estado cuyo valor es la palabra a enviar al servidor.
        LlamaModificarText es la funcion que modifica el estado "text"
        ConsultarApi es la funcion que envía la palabra al servidor.
    */
    return(
        <div data-testid="sender" className="container-fluid px-0">
            <div className="text-center py-2 bg-danger">
                <input type="text" value={text} onChange={llamaModificarText} placeholder="Insert Text" className="d-inline mx-3 form-control w-50"/>
                <button onClick={()=>{consultarApi(text)}} className="btn btn-primary px-4">Send</button>
            </div>
        </div>
    )
}

export default Sender