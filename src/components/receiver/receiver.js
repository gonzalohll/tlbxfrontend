import React from 'react'

const Receiver = ({primerResultado, segundoResultado, tercerResultado, esPalindromo}) => {
    /*
        Este componente recibe como props los resultados del request del servidor y los muestra.
    */
    return(
        <div data-testid="receiver" className="container-fluid">
            <div className="row justify-content-between">
                <div className="bg-white p-4 col-9 mx-auto mt-4" style={{minHeight:'70vh'}}>
                    <h5 className="text-secondary fw-normal">Results:</h5>
                    <div className="text-center">
                        {
                            (esPalindromo)?
                            <div data-testid="esPalindromoMostrado"  className="my-2 mx-auto w-50 alert alert-danger">{esPalindromo}</div>
                            :null
                        }
                        <input data-testid="primerResultadoMostrado" readOnly type="text" value={primerResultado} placeholder="Third Text" className="my-2 mx-auto form-control w-50"/>
                        <input data-testid="segundoResultadoMostrado" readOnly type="text" value={segundoResultado} placeholder="Second Text" className="my-2 mx-auto form-control w-50"/>
                        <input data-testid="tercerResultadoMostrado" readOnly type="text" value={tercerResultado} placeholder="First Text" className="my-2 mx-auto form-control w-50"/>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Receiver